const express = require('express');
const upload = require('multer')({dest: './views/assets/'});
const router = express.Router();
const isAuthenticated = require('../utils/authenticate');
const favoController = require('../controller/favourites');
router.get('/getAllFavourites', isAuthenticated, favoController.get);
router.post('/addFavourites', isAuthenticated, upload.single('picture'), favoController.post);

module.exports = router;