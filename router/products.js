const express = require('express');
const multer = require('multer');
const path = require('path');
/*const upload = require('multer')({dest: './views/assets/'});*/
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './views/assets//')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
});
const upload = multer({ storage: storage });
const router = express.Router();
const isAuthenticated = require('../utils/authenticate');
const productController = require('../controller/products');
router.get('/getAllProducts'/*, isAuthenticated*/, productController.get);
router.post('/addProduct'/*, isAuthenticated*/, upload.single('picture'), productController.post);
router.post('/searchProduct', isAuthenticated, upload.single('picture'), productController.postSearchProduct);
router.post('/getNearByProducts', isAuthenticated, upload.single('picture'), productController.postNearByProducts);

module.exports = router;