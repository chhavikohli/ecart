const express = require('express');
const morgan = require('morgan');
let fs = require('fs');
const  path = require('path');
const expressValidator = require('express-validator');
const register = require('./router/register');
const login = require('./router/login');
const products = require('./router/products');
const favourite = require('./router/favourites');
const password = require('./router/passwords');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
app.use(morgan('combined'));
app.use('/static', express.static('views'));

let accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});

app.use(morgan('combined', {stream: accessLogStream}));
app.use(expressValidator());
mongoose.connect('mongodb://localhost/cart');


app.use(bodyParser.json());
app.use('/signUp', register);
app.use('/login', login);
app.use('/', products);
app.use('/', favourite);
app.use('/', password);


app.listen('3000', function () {
    console.log('application started at 3000');
});
