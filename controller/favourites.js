const Favourites = require('../model/favourites');
const currentUser = require('../utils/currentUser');
let payload = {};
/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.get = function get(req, res, next) {
    let Id = req.query;
    Favourites.find({userId: Id.user}, function (err, productId) {
        res.send({success: true, data: productId});
    });
};


/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.post = function (req, res, next) {
    payload = {
        productId: req.body.productId,
    };
    payload.userId = currentUser.user.id;


    const favourites = new Favourites(payload);

    favourites.save(function (err, favourite) {
        if (err) throw err;

        return res.send({success: true, data: payload});
    });

};