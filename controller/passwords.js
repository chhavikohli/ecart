const User = require('../model/user');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const utils = require('../utils/passwordToken');
const currentUser = require('../utils/currentUser');

const nodemailer = require('nodemailer');
const async = require('async');

const saltRounds = 10;


/**
 * Password Checker
 * @param password
 * @returns {*}
 */
let passwordChecker = (password, userPassword) => {
    let hash = userPassword;
    return bcrypt.compareSync(password, hash);

};
/**
 * encrypting new password
 * @param password
 * @returns {*}
 */
let encryption = (password) => {
    let hash = bcrypt.hashSync(password, saltRounds);
    return hash;
};
/**
 * Post function to get user id and password
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
exports.post = function (req, res, next) {
    let newPassword = req.body.newPassword;
    if(req.body.newPassword === req.body.confirmPassword){
        User.findOne({email:currentUser.user.email}, function (err, user) {
            if (user) {
                let status = passwordChecker(req.body.oldPassword, user.password);
                if (status) {
                    newPassword = encryption(newPassword);
                    let oldData = {email: currentUser.user.email};
                    let newValues = {$set: {password: newPassword}};
                    User.updateOne(oldData, newValues, function (err, res) {
                        if (err) throw err;
                        console.log("password changed");
                    });

                    res.send({success: true, message: "password changed successfully"});

                    let smtpTransporter = nodemailer.createTransport({
                        service: 'Gmail',
                        auth: {
                            user: 'chhavikohli.evontech@gmail.com',
                            pass: 'chhavi@123'
                        }
                    });
                    let mailOptions = {
                        to: currentUser.user.email,
                        from: 'chhavikohli.evontech@gmail.com',
                        subject: 'ECart Password Reset',
                        text: 'Hello,\n\n' +
                        'This is a confirmation that the password for your account ' + currentUser.user.email + ' has just been changed.\n'
                    };

                    smtpTransporter.sendMail(mailOptions, function (err) {
                        console.log('mail sent');
                        done(err, 'done');
                    });
                } else {
                    return res.send({success: false, message: "incorrect password"});
                }

            } else {
                return res.send({success: false, message: "email id not found"});
            }

        });
    }else{
        return res.send({success: false, message: "confirm password and new password must be same"});
    }

};
/**
 * renderForgotPassword
 * @param req
 * @param res
 * @param next
 */
exports.postForgetPassword = function (req, res, next) {
    async.waterfall([
            (done) => {
                crypto.randomBytes(20, function (err, buf) {
                    let token = buf.toString('hex');
                    done(err, token);
                });
            },
            (token, done) => {
                console.log(token);
                let email = req.body.email.replace(/\s/g, '');
                /*email = email.toLowerCase();*/
                User.find({email: email}, (err, user) => {
                    if (!user[0]) {
                        return res.send({success: false, message: 'NO ACCOUNT WITH THAT EMAIL ADDRESS WAS FOUND'});
                    }
                    utils.token = {
                        passwordChecker: token,
                        email: email,
                    }
                    res.send({success: true, msg: 'reset password link has been sent to your account'})
                    let smtpTransporter = nodemailer.createTransport({
                        service: 'Gmail',
                        auth: {
                            user: 'chhavikohli.evontech@gmail.com',
                            pass: 'chhavi@123'
                        }
                    });
                    let mailOptions = {
                        to: req.body.email,
                        from: 'chhavikohli.evontech@gmail.com',
                        subject: 'ECart Password Reset',
                        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                        'http://' + req.headers.host + '/updatePassword/' + token + '\n\n' +
                        'If you did not request this, please ignore this email and your password will remain unchanged.\n'
                    };
                    smtpTransporter.sendMail(mailOptions, function (err) {
                        console.log('mail sent');
                        done(err, 'done');
                    });

                });
            }

        ],
        function (err) {
            if (err) return next(err);
        })

};


exports.postUpdatePassword = function (req, res, next) {
    let newPassword, email = utils.token.email;
    async.waterfall([
            function (done) {
                console.log(utils.token.passwordChecker,req.params.id);
                if (utils.token.passwordChecker && utils.token.passwordChecker === req.params.id) {

                    if (req.body.password === req.body.confirm) {
                        newPassword = encryption(req.body.password);
                        let oldData = {email: email};
                        var newvalues = {$set: {password: newPassword}};
                        User.updateOne(oldData, newvalues, function (err, res) {
                            if (err) throw err;
                            console.log("password changed");
                        });
                        res.send({success: true, message: 'password changed..please login to continue'})
                        let smtpTransporter = nodemailer.createTransport({
                            service: 'Gmail',
                            auth: {
                                user: 'chhavikohli.evontech@gmail.com',
                                pass: 'chhavi@123'
                            }
                        });
                        let mailOptions = {
                            to: email,
                            from: 'chhavikohli.evontech@gmail.com',
                            subject: 'ECart Password Reset',
                            text: 'Hello,\n\n' +
                            'This is a confirmation that the password for your account ' + email + ' has just been changed.\n'
                        };

                        smtpTransporter.sendMail(mailOptions, function (err) {
                            console.log('mail sent');
                            done(err, 'done');

                        });

                    } else {

                        return res.send({success: false, message: 'confirm password and new password must be same'});
                    }
                }
                else{
                    return res.send({success: false, message: 'unauthorized user'});
                }
            },

        ],
        function (err) {
            if (err) return next(err);
        })

};

