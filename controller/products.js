const Product = require('../model/product');
const currentUser = require('../utils/currentUser');
let payload = {};
const Geo = require('geo-nearby');

/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.get = function get(req, res, next) {
    let pageOptions = {
        page: req.query.offset || 0,
        limit: parseInt(req.query.limit) || 5
    };
    Product.count({}, function (err, count) {
        let flag = count;
        flag = flag - (pageOptions.page * pageOptions.limit);
        if (flag <= 0) {
            return res.send({
                success: true,
                message: "you have reached the limit",

            });
        }
        Product.find()
            .skip(pageOptions.page * pageOptions.limit)
            .limit(pageOptions.limit)
            .exec(function (err, doc) {
                if (err) {
                    res.status(500).json(err);
                    return;
                }
                ;


                res.status(200).json({
                    success: true,
                    "TotalCount": count,
                    "Array": doc
                });
            })
    })
};


/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.post = function (req, res, next) {

    payload = {
        name: req.body.name,
        cost: req.body.cost,
        description: req.body.description,
        type: req.body.type,
        sellerId: currentUser.user.id,
        lat: req.body.lat,
        long: req.body.long,
        location: req.body.location
    };
    if (req.file) {
        payload.picture = "http://192.168.1.124:3000/static/assets/" + req.file.filename;
    }


    const product = new Product(payload);

    product.save(function (err, product) {
        if (err) throw err;

        return res.send({success: true, data: payload});
    });

};
/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.postSearchProduct = function (req, res, next) {
    Product.find({type: req.body.productType}, function (err, product) {
        res.send({success: true, data: product});
    });
};

function uniqBy(a, key) {
    var seen = new Set();
    return a.filter(item => {
        var k = key(item);
        return seen.has(k) ? false : seen.add(k);
    });
}

/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.postNearByProducts = function (req, res, next) {
    let latitude = req.body.lat, longitude = req.body.long;
    let data = [];
    let nearByLocations;
    let nearByProducts = [];
    Product.find(function (err, product) {
        for (let i = 0; i < product.length; i++) {
            data[i] = [product[i].lat, product[i].long, product[i].location];
        }
        const dataSet = Geo.createCompactSet(data);
        const geo = new Geo(dataSet, {sorted: true});
        nearByLocations = geo.nearBy(latitude, longitude, 5000);
        if(!nearByLocations[0]){
            res.send({success: false, message: "no nearby products found"});
        } else{
            let b = uniqBy(nearByLocations, JSON.stringify);
            for (let i = 0; i < b.length; i++) {
                Product.find({location: nearByLocations[i].i}, function (err, product) {
                    for (let j = 0; j < product.length; j++) {
                        nearByProducts[j] = [product[j]._id, product[j].name, product[j].location];
                    }
                    if (i === b.length - 1) {
                        res.send({success: true, data: nearByProducts});
                    }


                })
            }
        }
    });
};
