const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const favouritesSchema = new Schema({
    userId: String,
    productId: String,
    created_at: Date,
    updated_at: Date,
    versionKey: false // You should be aware of the outcome after set to false
});

// the schema is useless so far

// we need to create a models using it
const favourites = mongoose.model('favourites', favouritesSchema);

// make this available to our users in our Node applications
module.exports = favourites;