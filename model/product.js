const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const productSchema = new Schema({
    name: String,
    cost: String,
    description: String,
    picture: String,
    type: String,
    sellerId: String,
    lat:String,
    long:String,
    location:String,
    created_at: Date,
    updated_at: Date,
    versionKey: false // You should be aware of the outcome after set to false
});

// the schema is useless so far

// we need to create a models using it
const Product = mongoose.model('Product', productSchema);

// make this available to our users in our Node applications
module.exports = Product;